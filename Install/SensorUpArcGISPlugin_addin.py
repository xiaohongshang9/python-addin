import imp
import os
from subprocess import Popen, PIPE
import shlex

import json
import itertools
import sys
import shutil

# Importing pyaddin-interval-caller module to allow evernts to be called by ArcMap
# See https://github.com/jasonbot/pyaddin-interval-caller
tickextension_info = imp.find_module('tickextension',
                                     [os.path.dirname(
                                         os.path.abspath(__file__))])
tickextension = imp.load_module('tickextension', *tickextension_info)

import numpy
import arcpy
import pythonaddins

# adding current folder as sys path
cwd = os.path.dirname(os.path.abspath(__file__))
sys.path.append(cwd)

# settting project path
os.environ.setdefault('PROJECT_ROOT', cwd)

project_root = os.environ['PROJECT_ROOT']
import utils

# Some public variables

refresh_view_automatically = True

debug = False

# Python executable
if os.path.exists('python_exe.txt'):
    with open('python_exe.txt') as exe_path:
        python_exe = exe_path.read().strip()
else:
    python_exe = 'python'


def getSearchDistanceInches(scale, selection_tolerance=3):
    """
    Returns the map distance in inches that corresponds to the
    input selection tolerance in pixels (default 3) at the
    specified map scale.
    """

    # DPI used by ArcMap when reporting scale
    return scale * selection_tolerance / 96.0


def extent_polygon(extent):
    """
    Get a arcpy.Polygon object with the extent provided
    """
    array = arcpy.Array()
    # Create the bounding box
    array.add(extent.lowerLeft)
    array.add(extent.lowerRight)
    array.add(extent.upperRight)
    array.add(extent.upperLeft)
    # ensure the polygon is closed
    array.add(extent.lowerLeft)
    # Create the polygon object
    polygon = arcpy.Polygon(array, extent.spatialReference)
    array.removeAll()
    return polygon


def display_time_series_data():
    """
    Display sensor things in local folder
    """
    fc_name_point = 'thingsTimeSeriesPoint'
    fc_path_point = os.path.join('in_memory', fc_name_point)

    fc_name_polygon = 'thingsTimeSeriesPolygon'

    fc_path_polygon = os.path.join('in_memory', fc_name_polygon)

    if arcpy.Exists(fc_path_point):
        arcpy.Delete_management(fc_path_point)
    if arcpy.Exists(fc_name_point):
        arcpy.Delete_management(fc_name_point)

    if arcpy.Exists(fc_path_polygon):
        arcpy.Delete_management(fc_path_polygon)

    if arcpy.Exists(fc_name_polygon):
        arcpy.Delete_management(fc_name_polygon)

    global ts_np_list_point
    global ts_list_polygon

    if len(ts_np_list_point) + len(ts_list_polygon) == 0:
        # This should not happen
        print('Please wait data is ready')
    else:
        sr = arcpy.SpatialReference(4326)
        if len(ts_np_list_point):
            arcpy.da.NumPyArrayToFeatureClass(
                ts_np_list_point, fc_path_point, ['XY'], sr)
            arcpy.MakeFeatureLayer_management(fc_path_point, fc_name_point)
            arcpy.ApplySymbologyFromLayer_management(
                fc_name_point, os.path.join(cwd, 'thingsTimeSeries.lyr'))

        if len(ts_list_polygon):
            arcpy.CopyFeatures_management(os.path.join(cwd, 'timeSeriesTemplate.shp'),
                                          fc_path_polygon)
            c = arcpy.da.InsertCursor(fc_path_polygon,
                                      ['SHAPE@', 'thingId', 'datastream',
                                       'timeTo', 'timeFrom', 'reading', 'norm'])

            for row in ts_list_polygon:
                c.insertRow(row)

            del c

            arcpy.ApplySymbologyFromLayer_management(
                fc_name_polygon, os.path.join(cwd, 'thingsTimeSeriesPolygon.lyr'))

        arcpy.RefreshActiveView()
        arcpy.RefreshTOC()


class SUAdvancedButton(object):

    """Implementation for SensorUp.advancedBtn (Button)"""

    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        global url
        global datastreams

        if not url:
            print('Please input url')

        if not datastreams:
            print('Please input datastreams')

        # Get current extent and convert to polygon
        mxd = arcpy.mapping.MapDocument('CURRENT')
        df = arcpy.mapping.ListDataFrames(mxd, "Layers")[0]

        # if the basemap has a spatial reference
        if df.spatialReference.name:
            extent_poly = extent_polygon(df.extent)

            # Construct spatial query string with current extent
            ext_json = json.loads(
                extent_poly.projectAs(arcpy.SpatialReference(4326)).JSON)['rings'][0]

            sq_str = "st_within(location, geography 'POLYGON (({0:.4f} {1:.4f}," \
                     " {2:.4f} {3:.4f}, {4:.4f} {5:.4f}, {6:.4f} {7:.4f}, {0:.4f} {1:.4f})) ')".format(
                         ext_json[0][0], ext_json[0][
                             1], ext_json[1][0], ext_json[1][1],
                         ext_json[2][0], ext_json[2][1], ext_json[3][0], ext_json[3][1])
        else:
            sq_str = ''

        # Call external download program
        dl_file = 'download_sensor_data.py'
        dl_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                               dl_file).replace('\\', '/')

        command = r'"%s" %s %s -d "%s" -a -s "%s"' % (python_exe, dl_path,
                                                          url, datastreams, sq_str)

        print(command)
        Popen(shlex.split(command), shell=True, stdout=PIPE)


class SUAppExtension(tickextension.TickExtension):

    """Implementation for SensorUp.extension (Extension)"""

    def __init__(self):
        self.enabled = True
        self.interval = 2  # Call every half second
        self.data_displayed = False
        self.frame = 0
        self.need_refresh = False
        self.things = {}
        self.things_loaded = False

        # for purpose of displaying time series data
        things_path = os.path.join(project_root, 'things')

        if os.path.exists(things_path):
            try:
                shutil.rmtree(things_path)
            except Exception as e:
                print('Failed to rm things folder, reason %s' % str(e))
                return
        try:
            proj_setting = utils.project_setting(cwd)
            proj_setting['thingObservations'] = {}
            # proj_setting['dataStatus'] = utils.DATASTATUS_NOTREADY
            utils.set_project_setting(proj_setting, cwd)

        except Exception as e:
            self.print_content(e)

    def startup(self):
        super(SUAppExtension, self).startup()

    def print_content(self, something):
        """
        onTime is not on main thread so can not print anything,
        if printing job is needed in onTimer function, use this function
        """
        print(something)

    def prepare_time_series_data(self):
        """
        Construct a numpy array ready to display in ArcMap
        """
        temp_ts_list_point = []

        global ts_list_polygon
        ts_list_polygon = []
        sr = arcpy.SpatialReference(4326)
        for key, val in self.things.iteritems():
            loc = val['coordinates']
            geom_type = val['geomType']
            for ds_id, datastream in val['Datastreams'].iteritems():
                ds_min = datastream['min']
                ds_max = datastream['max']
                for from_obsv, to_obsv in itertools.izip(datastream['Observations'],
                                                         datastream['Observations'][1:]):
                    if ds_min == ds_max:
                        obsv_norm = 0.5
                    else:
                        obsv_norm = (float(from_obsv['result']) - ds_min) / \
                                    (ds_max - ds_min)

                    from_time = str(
                        utils.from_utc(from_obsv['phenomenonTime']))
                    to_time = str(utils.from_utc(to_obsv['phenomenonTime']))
                    if geom_type.lower() == 'point':
                        temp_ts_list_point.append((key, ds_id,
                                                   (loc[0], loc[1]),
                                                   from_time, to_time,
                                                   float(from_obsv['result']),
                                                   obsv_norm))

                    elif geom_type.lower() == 'polygon':
                        vertex = arcpy.Point()
                        polygon_array = arcpy.Array()
                        for coord_pair in loc:
                            vertex.X = coord_pair[0]
                            vertex.Y = coord_pair[1]
                            polygon_array.add(vertex)
                        polygon = arcpy.Polygon(polygon_array, sr)
                        ts_list_polygon.append([polygon, key, ds_id,
                                                from_time, to_time,
                                                float(from_obsv['result']),
                                                obsv_norm])

            # TODO: py2/3 unicode

        # Construct numpy array
        dtype_point = numpy.dtype([('thingId', numpy.int32),
                                   ('datastreamId', numpy.int32),
                                   ('XY', '<f8', 2),
                                   # ('time', numpy.str),
                                   ('timeTo', '<U19'),
                                   ('timeFrom', '<U19'),
                                   ('reading', numpy.float),
                                   ('norm', numpy.float)])

        global ts_np_list_point
        ts_np_list_point = numpy.array(temp_ts_list_point, dtype_point)

    def display(self):
        """
        refresh current viewable layer with things_dict provided
        useful for passive displaying, not used now
        """

        # Layers operation
        fc_name = 'things'
        fc_path = os.path.join('in_memory', fc_name)

        if arcpy.Exists(fc_name):
            arcpy.Delete_management(fc_name)
        if arcpy.Exists(fc_path):
            arcpy.Delete_management(fc_path)

        # Convert self.things to numpy array and display
        things_np_list_point = []
        dtype = numpy.dtype([('idfield', numpy.int32),
                             ('XY', '<f8', 2),
                             ('ready', numpy.int32)])
        for i, (key, val) in enumerate(self.things.iteritems()):
            things_np_list_point.append((i, (val['coordinates'][0],
                                             val['coordinates'][1]),
                                         1))

        thing_np = numpy.array(things_np_list_point, dtype)

        sr = arcpy.SpatialReference(4326)
        arcpy.da.NumPyArrayToFeatureClass(thing_np, fc_path,
                                          ['XY'], sr)
        arcpy.MakeFeatureLayer_management(fc_path, fc_name)

        # Symbology
        # mxd = arcpy.mapping.MapDocument("current")
        # lyr = arxcpy.mapping.ListLayers(mxd, fc_name)[0]
        # lyr.symbologyType = 'UNIQUE_VALUES'
        # lyr.symbology.valueField = 'ready'
        # lyr.symbology.addAllValues()
        arcpy.RefreshActiveView()
        arcpy.RefreshTOC()

        self.need_refresh = False

    def frame_check(self):
        """
        Load data to self.things from things folder
        """
        proj_setting = utils.project_setting(cwd)

        if True:
            check_condition = True

            self.things = {}
            if check_condition:
                proj_setting = utils.project_setting(cwd)
                things_observations = proj_setting['thingObservations']

                for key, value in things_observations.iteritems():
                    if key not in self.things.keys():
                        self.need_refresh = True
                        # TODO: replace with a function
                        if os.path.exists(value):
                            with open(value) as fobj:
                                self.things[key] = json.load(fobj)
                            # cls=utils.ThingDecoder)
                # self.print_content(self.things)

                self.data_displayed = True

            else:
                # logged in but data now completely downloaded
                pass

            if self.things:
                # Thing is ready, we should enable the display button
                if not ts_displayBtn.enabled:
                    ts_displayBtn.enabled = True
            else:
                if ts_displayBtn.enabled:
                    ts_displayBtn.enabled = False

    def onTimer(self):
        # reset frame at 1000
        if self.frame == 1000:
            self.frame = 0

        self.frame += 1
        self.frame_check()

        # Not refreshing now
        if self.frame % 5 == 0:
            # refresh
            if self.need_refresh:
                if refresh_view_automatically:
                    pass
                    # self.display()

        if self.frame % 3 == 0:
            self.prepare_time_series_data()


class SUDisplayButton(object):

    """Implementation for SensorUp.ts_displayBtn (Button)"""

    def __init__(self):
        self.enabled = False
        self.checked = False

    def onClick(self):
        try:
            display_time_series_data()
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            print(e, exc_tb.tb_lineno)


class SUSettingButton(object):

    """Implementation for SensorUp.settingBtn (Button)"""

    def __init__(self):
        self.enabled = True
        self.checked = False

    def onClick(self):
        edit_settings_py = 'edit_settings.py'
        file_full_path = os.path.join(
            project_root, edit_settings_py).replace('\\', '/')
        command = 'python {}'.format(file_full_path)
        proc = Popen(shlex.split(command), shell=True, stdout=PIPE)


def run_download_process():
    global url
    global datastreams

    # validation process
    # TODO: Validate the url is in correct format
    if not url:
        print("Please input url")

    if not datastreams:
        print('Please input your datastream ids')

    gm_file = 'download_sensor_data.py'
    gm_path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           gm_file).replace('\\', '/')

    command = r'python %s "%s" -d "%s"' % (gm_path, url, datastreams)
    print(command)
    proc = Popen(shlex.split(command), shell=True, stdout=PIPE)


class UrlComboBox(object):

    """Implementation for SensorUp.urlCbox (ComboBox)"""

    def __init__(self):
        self.value = 'http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0/'
        global url
        url = self.value
        self.items = ['http://chashuhotpot.sensorup.com/OGCSensorThings/v1.0/']
        self.editable = True
        self.enabled = True
        width = 'WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW'
        self.dropdownWidth = width
        self.width = width
        self.url = ''

    def onSelChange(self, selection):
        pass

    def onEditChange(self, text):
        global url
        url = text

    def onFocus(self, focused):
        pass

    def onEnter(self):
        print('Download Observations')
        run_download_process()

    def refresh(self):
        pass


class DatastreamCombobox(object):

    """Implementation for SensorUp.dsCombobox (ComboBox)"""

    def __init__(self):
        self.items = []
        self.editable = True
        self.enabled = True
        self.dropdownWidth = 'WWWWWW'
        self.width = 'WWWWWW'
        global datastreams
        datastreams = ''

    def onSelChange(self, selection):
        pass

    def onEditChange(self, text):
        global datastreams
        datastreams = text

    def onFocus(self, focused):
        pass

    def onEnter(self):
        print('Download Observations')
        run_download_process()

    def refresh(self):
        pass
