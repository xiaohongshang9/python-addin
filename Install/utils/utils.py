import datetime
import json
import requests
from string import Template
import os
import sys
import copy

# const strings
STATUS_NOT_LOGGEDIN = 'not logged in'
STATUS_LOGGEDIN = 'logged in'
DATASTATUS_NOTREADY = 'not ready'
DATASTATUS_DOWNLOADING = 'downloading'
DATASTATUS_READY = 'ready'

cwd = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if 'PROJECT_ROOT' not in os.environ:
    os.environ.setdefault('PROJECT_ROOT', cwd)

project_root = os.environ['PROJECT_ROOT']

def from_utc(utc_time, fmt="%Y-%m-%dT%H:%M:%S.%f"):
    """
    Convert UTC time string to time.struct_time
    """
    # change datetime.datetime to time, return time.struct_time type
    if utc_time.endswith('Z'):
        fmt += 'Z'
    try:
        dtime = datetime.datetime.strptime(utc_time, fmt)
        return dtime

    except Exception:
        fmt = "%Y-%m-%dT%H:%M:%S"
        dtime = datetime.datetime.strptime(utc_time, fmt)
        return dtime

# TODO: how to take of datetime using nested?


class ThingEncoder(json.JSONEncoder):

    """JSON Encoder for a thing """

    def default(self, obj):

        # TODO: probably it is not safe, Thing is unknown to this module yet
        # Probably let user provide its own Enc/Decoder?
        if isinstance(obj, Thing):
            return {'description': obj.desc,
                    'properties': obj.prop,
                    'Locations': [obj.Location],
                    'Datastreams': obj.datastream,
                    'id': obj.thing_id,
                    'obsv_min': obj.obsv_min,
                    'obsv_max': obj.obsv_max}

        elif isinstance(obj, datetime.datetime):
            return obj.isoformat()


class ThingDecoder(json.JSONDecoder):

    """JSON Decoder for a thing"""

    def decode(self, obj):
        if isinstance(obj, datetime.datetime):
            return from_utc(obj)
        return super(ThingDecoder, self).decode(obj)


class Thing(object):

    '''
    Representation of a Thing(Sensor)
    '''

    def __init__(self, desc, loc, thing_id, base_url='', thing_dir='',
                 properties='', proj_setting={}):
        super(Thing, self).__init__()
        self.base_url = base_url
        self.url_ds = Template(
            self.base_url + 'Things($thing_id)/Datastreams?$$expand=ObservedProperty,Sensor')
        self.url_obs = Template(
            self.base_url + 'Datastreams($ds_id)/Observations')
        self.desc = desc
        self.prop = properties
        self.proj_setting = proj_setting
        self.keys_dict = self.proj_setting['keysDict']
        self.Location = loc
        if 'location' in loc:
            self.location = loc[self.keys_dict['location']]
        else:
            self.location = loc['feature']

        self.coord = self.location['coordinates']
        self.geom_type = self.location['type']
        self.thing_id = thing_id
        self.datastream = {}
        self.thing_dir = thing_dir
        self.obsv_max = - sys.maxint - 1
        self.obsv_min = sys.maxint

    def add_ds(self, ds):
        '''Adding a datastream to current thing'''
        if ds[self.keys_dict['id']] not in self.datastream:
            self.datastream[ds[self.keys_dict['id']]] = copy.copy(ds)
            self.datastream[ds[self.keys_dict['id']]]['Observations'] = []
            self.datastream[ds[self.keys_dict['id']]]['min'] = sys.maxint
            self.datastream[ds[self.keys_dict['id']]]['max'] = - sys.maxint - 1

        return

    def add_obss(self, ds, obsv):
        '''Adding a observation to current thing, updating min/max value'''

        # In case people forgot to call add_ds
        if ds[self.keys_dict['id']] not in self.datastream:
            self.datastream[ds[self.keys_dict['id']]] = ds
            self.datastream[ds[self.keys_dict['id']]]['Observations'] = []

            self.datastream[ds[self.keys_dict['id']]]['min'] = sys.maxint
            self.datastream[ds[self.keys_dict['id']]]['max'] = - sys.maxint - 1

        # update min/max
        self.datastream[ds[self.keys_dict['id']]]['Observations'].append(obsv)

        # TODO: Observation will always be numbers?
        try:
            if float(obsv['result']) < self.datastream[ds[self.keys_dict['id']]]['min']:
                self.datastream[ds[self.keys_dict['id']]]['min'] = float(obsv['result'])
            if float(obsv['result']) > self.datastream[ds[self.keys_dict['id']]]['max']:
                self.datastream[ds[self.keys_dict['id']]]['max'] = float(obsv['result'])
        except Exception as e:
            # Some observations are not float, dont show them
            pass

    def init_obss(self):
        '''
        Init observations for this thing
        '''

        # Get list of datastream
        # print(self.url_ds.substitute(thing_id=self.thing_id))

        ds_url = self.url_ds.substitute(thing_id=self.thing_id)
        res = requests.get(ds_url, timeout=1)
        dss = res.json()

        # Possible error as return value
        if self.keys_dict['value'] in dss:
            for ds_value in dss[self.keys_dict['value']]:
                # Get obs value for this ds
                self.add_ds(ds_value)
                obss = requests.get(self.url_obs.substitute(
                    ds_id=ds_value[self.keys_dict['id']]), timeout=1).json()
                if self.keys_dict['value'] in obss:
                    for obs in obss[self.keys_dict['value']]:

                        # filter out keys that are not useful
                        new_obss = {key: obs[key] for key in [self.keys_dict['result'],
                                                              self.keys_dict['phenomenonTime'],
                                                              self.keys_dict['id']]}
                        # time = datetime.strptime(obs['phenomenonTime'][:10],
                        #                          '%Y-%m-%d')
                        self.add_obss(ds_value, new_obss)

        # sort observations based on time

    def dumps(self):
        '''dump things to txt files'''

        self.thing_file_name = os.path.join(self.thing_dir,
                                            'thing_%s.txt' % self.thing_id)
        thing_text = json.dumps(self, cls=ThingEncoder)
        with open(self.thing_file_name, 'w') as fobj:
            fobj.write(thing_text)
