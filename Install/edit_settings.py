"""
This module is the GUI used for updating project settings
"""
import Tkinter as tk
import os
import functools
import collections
import time
import copy
import ttk
from collections import OrderedDict
import json
import utils
from utils import gui


cwd = os.path.dirname(os.path.abspath(__file__))
if 'PROJECT_ROOT' not in os.environ:
    os.environ.setdefault('PROJECT_ROOT', cwd)


def save_setting():
    proj_setting['maxReadings'] = int(max_reading_entry.get())
    proj_setting['defaultUrl'] = url_entry.get()
    proj_setting['layerFile'] = layer_entry.get()
    try:
        utils.set_project_setting(proj_setting, cwd)
        status_label.configure(text='Successful')
        status_label.pack()
    except Exception as e:
        text = 'Failed: {}'.format(e)
        status_label.configure(text=text)
        status_label.pack()


if __name__ == '__main__':
    proj_setting = utils.project_setting(cwd)
    root = tk.Tk()
    root.wm_title('Edit SensorUp Settings')
    upper_frame = ttk.Frame(root)
    upper_frame.pack(anchor='nw')
    max_reading_label = tk.Label(upper_frame, text='Max Readings', borderwidth=0,
                                 padx=5, pady=5, anchor=tk.W)
    max_reading_label.grid(row=0, column=0)

    max_reading_entry = gui.IntegerEntry(upper_frame, int(proj_setting['maxReadings']),
                                         width=50)
    max_reading_entry.grid(row=0, column=1, sticky='ew')

    url_label = tk.Label(upper_frame, text='Default Url', borderwidth=0,
                         padx=5, pady=5, anchor=tk.W)
    url_label.grid(row=1, column=0)

    url_value = tk.StringVar()
    url_value.set(proj_setting['defaultUrl'])
    url_entry = tk.Entry(upper_frame, textvariable=url_value, width=50)
    url_entry.grid(row=1, column=1, sticky='ew')

    layer_label = tk.Label(upper_frame, text='Layer File', borderwidth=0,
                           padx=5, pady=5, anchor=tk.W)
    layer_label.grid(row=2, column=0)

    layer_value = tk.StringVar()
    layer_value.set(proj_setting['layerFile'])
    layer_entry = tk.Entry(upper_frame, textvariable=layer_value, width=50)
    layer_entry.grid(row=2, column=1, sticky='ew')

    mid_frame = tk.Frame(root)
    mid_frame.pack()
    status_label = tk.Label(mid_frame)

    save_button = tk.Button(root, text='Save Settings', command=save_setting)
    save_button.pack()

    root.mainloop()
